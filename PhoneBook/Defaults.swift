//
//  Defaults.swift
//  PhoneBook
//
//  Created by Lallu Sukendh on 03/06/18.
//  Copyright © 2018 SukendhP. All rights reserved.
//

import Foundation
import UIKit

class Defaults {
    // Common methods
    class func isValidEmail(email:String) -> Bool {
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = email as NSString
            let results = regex.matches(in: email, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    // Alert view for common usage
    class func showAlertMessage (target: UIViewController, title: String?, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        target.present(alert, animated: true, completion: nil)
    }
}
