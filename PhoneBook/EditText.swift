//
//  EditText.swift
//  PhoneBook
//
//  Created by Lallu Sukendh on 02/06/18.
//  Copyright © 2018 SukendhP. All rights reserved.
//

import Foundation
import UIKit

class EditText: UIView, UITextFieldDelegate {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    var maxLength: Int = 100
    
    @IBInspectable var titleText: String = "Title" {
        didSet {
            self.titleLabel.text = titleText
        }
    }
    @IBInspectable var placeHolder: String = "Placeholder" {
        didSet {
            self.textField.placeholder = placeHolder
        }
    }
    
    @IBInspectable var isEnabled: Bool = true {
        didSet {
            self.textField.isUserInteractionEnabled = isEnabled
        }
    }
    
    @IBInspectable var MaxLenght: Int = 100 {
        didSet {
            maxLength = MaxLenght
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let _ = loadViewFromNib()
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle.init(for: type(of: self))
        let nibName = String(describing: EditText.self)
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = bounds
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        addSubview(view)
        return view
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
}
