//
//  HomeViewController.swift
//  PhoneBook
//
//  Created by Lallu Sukendh on 01/06/18.
//  Copyright © 2018 SukendhP. All rights reserved.
//

import UIKit
import CoreData

class HomeViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var addContactButton: UIButton!
    
    
    var contentArray: [Contact] = []
    var filtered: [Contact] = []
    var searchActive : Bool = false
    let cellId = "home_cell"
    var selectedIndex: Int!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let nib = UINib.init(nibName: "HomeTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: cellId)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.setNavigationBarHidden(true, animated: false)
        fetchDataFromCoreData()
    }
    
    func fetchDataFromCoreData() {
        let fetchRequest: NSFetchRequest<Contact> = Contact.fetchRequest()
        do {
            let contacts = try PersistanceService.context.fetch(fetchRequest)
            self.contentArray = contacts
        } catch {}
        tableView.reloadData()
    }

    @IBAction func addTapped(_ sender: UIButton) {
        let addContactVC = AddContactViewController()
        addContactVC.delegate = self
        addContactVC.count = contentArray.count
        self.navigationController?.present(addContactVC, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension HomeViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchActive {
            return filtered.count
        } else {
            return contentArray.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! HomeTableViewCell
        let cellItem: Contact!
        if searchActive {
            cellItem = filtered[indexPath.row]
        } else {
            cellItem = contentArray[indexPath.row]
        }
        
        
        cell.nameLabel.text = cellItem.firstName
        if cellItem.image != nil {
            cell.cellImage.image = UIImage(data: cellItem.image! as Data)
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellItem: Contact!
        if searchActive {
            cellItem = filtered[indexPath.row]
        } else {
            cellItem = contentArray[indexPath.row]
        }
        selectedIndex = indexPath.row
        let viewContactVC = ViewContactViewController()
        viewContactVC.contactData = cellItem
        self.navigationController?.pushViewController(viewContactVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 48
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView,
                   trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        let deleteAction = UIContextualAction(style: .normal, title:  "Delete", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            // delete from coredata.
            
            let context = PersistanceService.context
            context.delete(self.contentArray[indexPath.row])
            do {
                try context.save()
            } catch {
                return success(false)
            }
            self.contentArray.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            tableView.reloadData()
            success(true)
        })
        deleteAction.backgroundColor = .red
        
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }


}

extension HomeViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchActive = false
        tableView.reloadData()
        searchBar.endEditing(true)
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.resignFirstResponder()
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.endEditing(true)
        searchBar.resignFirstResponder()
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filtered.removeAll()
        let predicateString = searchBar.text!
        filtered = contentArray.filter({$0.firstName!.range(of: predicateString) != nil})
        filtered.sort{$0.firstName! < $1.firstName!}
        searchActive = (filtered.count == 0) ? false: true
        self.tableView.reloadData()
    }
}

extension HomeViewController: addContactDelegate {
    func addContact(data: Contact) {
        fetchDataFromCoreData()
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
