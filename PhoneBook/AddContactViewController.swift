//
//  AddContactViewController.swift
//  PhoneBook
//
//  Created by Lallu Sukendh on 02/06/18.
//  Copyright © 2018 SukendhP. All rights reserved.
//

import UIKit
import CoreData

protocol addContactDelegate {
    func addContact(data: Contact)
}

class AddContactViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var firstNameField: EditText!
    @IBOutlet weak var lastNameField: EditText!
    @IBOutlet weak var phoneField: EditText!
    @IBOutlet weak var emailField: EditText!
    @IBOutlet weak var addressTextView: UITextView!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var imageEditButton: UIButton!
    @IBOutlet weak var cencelButton: UIButton!
    @IBOutlet weak var viewHeightConstraint: NSLayoutConstraint! // 730
    
    var delegate: addContactDelegate?
    var count: Int!
    var contact: Contact?
    let pickerController = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.hideKeyboardWhenTappedAround()
        
        phoneField.textField.keyboardType = UIKeyboardType.phonePad
        emailField.textField.keyboardType = UIKeyboardType.emailAddress
        
        // Setting imageview in circle shape.
        profileImageView.layer.borderWidth = 2
        profileImageView.layer.masksToBounds = false
        profileImageView.layer.borderColor = UIColor.white.cgColor
        profileImageView.layer.cornerRadius = profileImageView.frame.height/2
        profileImageView.clipsToBounds = true
        profileImageView.image = #imageLiteral(resourceName: "ic_placeholder")
        
        if contact != nil {
            showContactData()
            titleLabel.text = "Edit Contact"
        }
        emailField.textField.delegate = self
        addressTextView.delegate = self
    }
    
    func showContactData() {
        firstNameField.textField.text = contact?.firstName
        lastNameField.textField.text = contact?.lastName
        phoneField.textField.text = contact?.phoneNo
        emailField.textField.text = contact?.email
        addressTextView.text = contact?.address
        profileImageView.image = UIImage(data: contact!.image! as Data)
    }
    
    // UIButton Action methods.
    @IBAction func imageEditButtonTapped(_ sender: UIButton) {
        let alert = UIAlertController(title: "", message:"Please choose an option ", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { action in
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                self.pickerController.delegate = self;
                self.pickerController.sourceType = .camera
                self.present(self.pickerController, animated: true, completion: nil)
            }
        }))
        alert.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { action in
            self.pickerController.delegate = self
            self.pickerController.sourceType = .photoLibrary
            self.pickerController.allowsEditing = true
            self.present(self.pickerController, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }

    @IBAction func doneButtonTapped(_ sender: UIButton) {
        if isfieldsValidated() {
            // get iamgeData
            let image = profileImageView.image
            let imageData = UIImageJPEGRepresentation(image!, 0.7)
            
            // saving
            if contact == nil {
                let contact = Contact(context: PersistanceService.context)
                contact.firstName = firstNameField.textField.text
                contact.lastName = lastNameField.textField.text
                contact.phoneNo = phoneField.textField.text
                contact.email = emailField.textField.text
                contact.image = imageData as NSData?
                contact.address = addressTextView.text
                contact.id = Int16(arc4random_uniform(42))
                delegate?.addContact(data: contact)
            } else {
                contact?.firstName = firstNameField.textField.text
                contact?.lastName = lastNameField.textField.text
                contact?.phoneNo = phoneField.textField.text
                contact?.email = emailField.textField.text
                contact?.image = imageData as NSData?
                contact?.address = addressTextView.text
                delegate?.addContact(data: contact!)
            }
            
            PersistanceService.saveContext()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func cancelTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Validation methods
    
    func isfieldsValidated() -> Bool {
        if (firstNameField.textField.text?.isEmpty)! {
            Defaults.showAlertMessage(target: self, title:"", message: "Please enter first name.")
            return false
        }
        else if (phoneField.textField.text?.isEmpty)! {
            Defaults.showAlertMessage(target: self, title:"", message: "Please enter mobile number.")
            return false
        }
        else if !(emailField.textField.text?.isEmpty)! {
            var bool: Bool = true
            if !Defaults.isValidEmail(email: emailField.textField.text!) {
                Defaults.showAlertMessage(target: self, title:"", message: "Please enter valid email.")
                bool = false
            }
            return bool
        }
        else {
            return true
        }
    }
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension AddContactViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var selectedImage: UIImage?
        if let editedImage = info["UIImagePickerControllerEditedImage"]   as? UIImage {
            selectedImage = editedImage
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            selectedImage = originalImage
        }
        if let selectedImages = selectedImage {
            profileImageView.image = selectedImages
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension AddContactViewController: UITextFieldDelegate, UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.viewHeightConstraint.constant = self.viewHeightConstraint.constant + 130
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.viewHeightConstraint.constant = self.viewHeightConstraint.constant - 130
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.viewHeightConstraint.constant = self.viewHeightConstraint.constant + 130
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.viewHeightConstraint.constant = self.viewHeightConstraint.constant - 130
    }
}
