//
//  Contact+CoreDataProperties.swift
//  PhoneBook
//
//  Created by Lallu Sukendh on 03/06/18.
//  Copyright © 2018 SukendhP. All rights reserved.
//
//

import Foundation
import CoreData


extension Contact {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Contact> {
        return NSFetchRequest<Contact>(entityName: "Contact")
    }

    @NSManaged public var firstName: String?
    @NSManaged public var lastName: String?
    @NSManaged public var phoneNo: String?
    @NSManaged public var email: String?
    @NSManaged public var address: String?
    @NSManaged public var image: NSData?
    @NSManaged public var id: Int16

}
