//
//  HomeTableViewCell.swift
//  PhoneBook
//
//  Created by Lallu Sukendh on 01/06/18.
//  Copyright © 2018 SukendhP. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var cellImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        cellImage.layer.borderWidth = 2
        cellImage.layer.masksToBounds = false
        cellImage.layer.borderColor = UIColor.white.cgColor
        cellImage.layer.cornerRadius = cellImage.frame.height/2
        cellImage.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
