//
//  ViewContactViewController.swift
//  PhoneBook
//
//  Created by Lallu Sukendh on 02/06/18.
//  Copyright © 2018 SukendhP. All rights reserved.
//

import UIKit

class ViewContactViewController: UIViewController {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mobileLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var address: UITextView!
    @IBOutlet weak var deleteContact: UIButton!
    
    
    var contactData: Contact!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(editTapped))
        
        profileImageView.layer.borderWidth = 2
        profileImageView.layer.masksToBounds = false
        profileImageView.layer.borderColor = UIColor.white.cgColor
        profileImageView.layer.cornerRadius = profileImageView.frame.height/2
        profileImageView.clipsToBounds = true
        
        showContactData()
    }
    
    @IBAction func deleteButtonTapped(_ sender: UIButton) {
        let alert = UIAlertController(title: "Are you sure?", message: "Do you want to delete this contact?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            let context = PersistanceService.context
            context.delete(self.contactData)
            do {
                try context.save()
            } catch {}
            self.navigationController?.popToRootViewController(animated: true)
        }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @objc func editTapped() {
        let addContactVC = AddContactViewController()
        addContactVC.contact = contactData
        addContactVC.delegate = self
        self.navigationController?.present(addContactVC, animated: true, completion: nil)
    }
    
    func showContactData() {
        profileImageView.image = UIImage(data: contactData.image! as Data)
        nameLabel.text = contactData.firstName! + " " + contactData.lastName!
        mobileLabel.text = contactData.phoneNo
        emailLabel.text = contactData.email
        address.text = contactData.address
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ViewContactViewController: addContactDelegate {
    func addContact(data: Contact) {
        contactData = data
        showContactData()
    }
}



